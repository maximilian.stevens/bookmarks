package ch.bbw.backend.mockup;

import ch.bbw.backend.model.Bookmark;
import ch.bbw.backend.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepositoryMock {

    private final ArrayList<User> users;

    public UserRepositoryMock() {
        users = new ArrayList<User>() {{
            add(new User(1, "test@gmail.com", "1234", Collections.singletonList(new Bookmark(198663, "The Maze Runner", "movie"))));
            add(new User(2, "test1@gmail.com", "1234", Collections.singletonList(new Bookmark(198663, "The Maze Runner", "movie"))));
            add(new User(3, "test2@gmail.com", "1234", Collections.singletonList(new Bookmark(198663, "The Maze Runner", "movie"))));
            add(new User(4, "test3@gmail.com", "1234", Collections.singletonList(new Bookmark(198663, "The Maze Runner", "movie"))));
            add(new User(5, "test4@gmail.com", "1234", Collections.singletonList(new Bookmark(198663, "The Maze Runner", "movie"))));
        }};
    }

    public List<User> list() {
        return users;
    }

    public User get(Integer id) {
        Optional<User> test = users.stream().filter(user -> user.getId().equals(id)).findFirst();
        return test.orElse(null);
    }

    public User create(User user) {
        user.setId(1);
        if (!users.isEmpty()) {
            int lastId = users.stream()
                    .max(Comparator.comparingInt(User::getId))
                    .get()
                    .getId();
            user.setId(lastId + 1);
        }
        users.add(user);
        return user;
    }

    public User update(User user) {
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId().equals(user.getId())) {
                users.set(i, user);
                return user;
            }
        }
        return null;
    }

    public boolean delete(int id) {
        return users.remove(get(id));
    }

    public User login(User user) {
        Optional<User> result = users
                .stream()
                .filter(user1 -> user1.getUsername().equals(user.getUsername())).findFirst()
                .filter(user1 -> user1.getPassword().equals(user.getPassword()));
        return result.orElse(null);
    }

    public User signup(User user) {
        user.setBookmarks(Collections.emptyList());
        Optional<User> result = users
                .stream()
                .filter(user1 -> user1.getUsername().equals(user.getUsername())).findFirst();
        return result.isPresent() ? null : create(user);
    }
}
