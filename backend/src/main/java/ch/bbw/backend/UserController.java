package ch.bbw.backend;

import ch.bbw.backend.mockup.UserRepositoryMock;
import ch.bbw.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {

    final UserRepositoryMock userRepositoryMock;

    @Autowired
    public UserController(UserRepositoryMock userRepositoryMock) {
        this.userRepositoryMock = userRepositoryMock;
    }

    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userRepositoryMock.list();
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<User> getUserById(@PathVariable int id) {
        User result = userRepositoryMock.get(id);
        return result == null ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/signup")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> signup(@RequestBody User user) {
        User result = userRepositoryMock.signup(user);
        return result == null ? new ResponseEntity<>(HttpStatus.ALREADY_REPORTED) : new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody User user) {
        User result = userRepositoryMock.login(user);
        return result == null ? new ResponseEntity<>(HttpStatus.NOT_FOUND) : new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    public User updateUser(@RequestBody User user, @PathVariable int id) {
        user.setId(id);
        return userRepositoryMock.update(user);
    }

    @DeleteMapping("/users/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<HttpStatus> deleteUser(@PathVariable int id) {
        boolean result = userRepositoryMock.delete(id);
        return result ? new ResponseEntity<>(HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}
