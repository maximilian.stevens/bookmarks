package ch.bbw.backend.model;

import java.util.List;

public class User {

    private Integer id;
    private String username;
    private String password;
    private List<Bookmark> bookmarks;

    public User(Integer id, String username, String password, List<Bookmark> bookmarks) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.bookmarks = bookmarks;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", bookmarks='" + bookmarks + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Bookmark> getBookmarks() {
        return bookmarks;
    }

    public void setBookmarks(List<Bookmark> bookmarks) {
        this.bookmarks = bookmarks;
    }
}
