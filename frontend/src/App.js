import React, {useEffect, useState} from 'react';
import {Redirect, Route, Switch, useHistory} from 'react-router-dom';
import './App.css';
import 'react-notifications/lib/notifications.css';
import Login from "./pages/Login";
import SignUp from "./pages/Signup";
import Header from "./components/Header";
import Bookmarks from "./pages/Bookmarks";
import Media from "./pages/Media";
import {getBookmarksFromUser} from "./api/apiCalls";
import {NotificationContainer} from "react-notifications";

const App = () => {

    const [bookmarks, setBookmarks] = useState([]);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const history = useHistory();
    const backgroundStyles = {
        background: "url(https://i.pinimg.com/originals/0e/9e/88/0e9e8812f01f82650833264673bf51ed.jpg)",
        backgroundSize: "cover",
    }

    useEffect(() => {
        const user = JSON.parse(localStorage.getItem("user"));
        if (user?.bookmarks) {
            setIsLoggedIn(true);
            getBookmarksFromUser(user).then(setBookmarks);
        }
    }, [])

    const handleUserAuthentication = async (user) => {
        localStorage.setItem("user", JSON.stringify(user));
        const actualBookmarks = await getBookmarksFromUser(user);
        setBookmarks(actualBookmarks);
        history.push("/");
        setIsLoggedIn(true);
    }

    return (
        <div className="App">
            <Switch>
                {isLoggedIn ?
                    <>
                        <NotificationContainer/>
                        <Header setIsLoggedIn={setIsLoggedIn} setBookmarks={setBookmarks}/>
                        <Route exact={true} path="/">
                            <Media bookmarks={bookmarks} setBookmarks={setBookmarks}/>
                        </Route>
                        <Route exact={true} path="/series">
                            <Media bookmarks={bookmarks} setBookmarks={setBookmarks}/>
                        </Route>
                        <Route exact={true} path="/bookmarks">
                            <Bookmarks bookmarks={bookmarks} setBookmarks={setBookmarks}/>
                        </Route>
                        <Route exact={true} path="/login">
                            <Redirect to="/"/>
                        </Route>
                    </>
                    :
                    <>
                        <div className="auth-wrapper" style={backgroundStyles}>
                            <Route exact={true} path="/login">
                                <Login handleUserAuthentication={handleUserAuthentication}
                                       setIsLoggedIn={setIsLoggedIn}/>
                            </Route>
                            <Route exact={true} path="/sign-up">
                                <SignUp handleUserAuthentication={handleUserAuthentication}
                                        setIsLoggedIn={setIsLoggedIn}/>
                            </Route>
                            <Route exact={true} path="/">
                                <Redirect to="/login"/>
                            </Route>
                        </div>
                    </>
                }
            </Switch>
        </div>
    );
}

export default App;
