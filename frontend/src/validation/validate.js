const validate = (
    email,
    password,
    confirmPassword,
) => {

    const validateEmail = () => {
        const expression = new RegExp(
            /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$/
        );
        return expression.test(String(email).toLowerCase());
    };

    const validatePassword = () => {
        return password.length >= 4 && confirmPassword === password;
    };

    if (validateEmail() && validatePassword()) {
        return true;
    }

    return !!(validateEmail() && validatePassword());
};

export default validate;
