import React, {useEffect, useState} from 'react';
import Button from "@material-ui/core/Button"
import Rating from "@material-ui/lab/Rating";
import {getBookmarksFromUser, updateUser} from "../api/apiCalls";
import {NotificationManager} from "react-notifications";

const MediaCard = ({media, bookmarks, setBookmarks}) => {

    const [doesBookmarkExist, setDoesBookmarkExist] = useState(false);
    const imgUrl = media.poster_path !== null
        ? `https://image.tmdb.org/t/p/original/${media.poster_path}`
        : "https://davco-online.com.sg/media/catalog/product/cache/1/small_image/300x400/9df78eab33525d08d6e5fb8d27136e95/placeholder/default/No_image_available_1.jpg";


    useEffect(() => {
        if (media.title === undefined || media.type === 'tv') {
            media.type = 'tv';
            media.title = media.name;
        } else {
            media.type = 'movie';
        }
    }, [media])

    useEffect(() => {
        bookmarks.forEach(bookmark => {
            if (bookmark.id === media.id) {
                setDoesBookmarkExist(true);
            }
        })
    }, [bookmarks, media.id]);

    const update = async () => {
        let user = JSON.parse(localStorage.getItem("user"));
        user = doesBookmarkExist ? removeBookmarkFromUser(user) : addBookmarkToUser(user);
        const updatedUser = await updateUser(user);
        localStorage.setItem("user", JSON.stringify(updatedUser))
        const updatedBookmarks = await getBookmarksFromUser(updatedUser);
        setBookmarks(updatedBookmarks);
        setDoesBookmarkExist(!doesBookmarkExist);
    }

    const addBookmarkToUser = (user) => {
        NotificationManager.success(`${media.title} has been added to the bookmarks`, "Media added!", 2000)
        user.bookmarks = [...user.bookmarks, media];
        return user;
    }

    const removeBookmarkFromUser = (user) => {
        NotificationManager.error(`${media.title} has been removed from the bookmarks`, "Media removed!", 2000)
        user.bookmarks = [...user.bookmarks.filter(item => item.id !== media.id)];
        return user;
    }

    const convertToFormattedNumber = (term) => {
        return term.toString().replace(/\B(?=(\d{3})+(?!\d))/g, "'")
    }

    return (
        <>
            <div className="mediaCard">
                <div className="mediaCard__image-container">
                    <img
                        alt="Can't seem to find what you need..."
                        className="mediaCard__image"
                        src={imgUrl}
                    />
                </div>
                <div className="mediaCard__content">
                    <h1>
                        {media.title}
                    </h1>
                    <h4>
                        Views: {convertToFormattedNumber(media.popularity * 1000)}
                    </h4>
                    <Rating
                        className="mediaCard__rating"
                        readOnly={true}
                        name="simple-controlled"
                        max={10}
                        value={media.vote_average}
                    />
                    <h4>
                        Votes: {convertToFormattedNumber(media.vote_count)} (Avg: {media.vote_average})
                    </h4>
                    <h4>
                        Description
                    </h4>
                    <p className="mediaCard__description">
                        {media.overview}
                    </p>
                    <Button
                        className={!doesBookmarkExist ? "mediaCard__button" : null}
                        variant="contained"
                        color="secondary"
                        onClick={update}
                    >
                        {doesBookmarkExist ? "Remove from Bookmarks" : "Add to Bookmarks"}
                    </Button>
                </div>
            </div>
            <hr className="mediaCard__hr"/>
        </>
    );
}

export default MediaCard;