import React from "react";
import {Link} from 'react-router-dom';
import {Nav, Navbar, NavDropdown} from "react-bootstrap";
import {CgProfile} from "react-icons/cg";

const Header = ({setBookmarks, setIsLoggedIn}) => {

    const user = JSON.parse(localStorage.getItem("user"));
    const dropdown = (
        <>
            <CgProfile size="40px"/>
            <span className="navbar__username">{user.username}</span>
        </>
    );

    const logout = () => {
        localStorage.clear();
        setBookmarks([]);
        setIsLoggedIn(false);
    }

    return (
        <Navbar sticky="top" collapseOnSelect expand="sm" bg="dark" variant="dark">
            <Nav className="mr-auto">
                <Link className="nav-link" to="/">
                    Movies
                </Link>
                <Link className="nav-link" to="/series">
                    Series
                </Link>
                <Link className="nav-link" to="/bookmarks">
                    Bookmarks
                </Link>
            </Nav>
            <Nav>
                <NavDropdown title={dropdown} id="collapsible-nav-dropdown">
                    <Link
                        className="dropdown-item"
                        onClick={logout}
                        to="/"
                    >
                        Log out
                    </Link>
                </NavDropdown>
            </Nav>
        </Navbar>
    );
}
export default Header;