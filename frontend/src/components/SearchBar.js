import React from 'react';
import Button from '@material-ui/core/Button';

const SearchBar = ({searchTerm, setSearchTerm, type, onFormSubmit}) => {

    const submit = (e) => {
        e.preventDefault();
        searchTerm !== '' && onFormSubmit(searchTerm);
    }

    return (
        <div className="ui segment">
            <form onSubmit={submit} className="ui form">
                <div className="searchbar">
                    <label>
                        Search {type}
                    </label>
                    <div className="searchbar__input-container">
                        <input
                            className="searchbar__input"
                            type="text"
                            onChange={(e) => setSearchTerm(e.target.value)}
                            value={searchTerm}
                        />
                        <Button
                            className="searchbar__button"
                            type="submit"
                            variant="contained"
                            disabled={searchTerm === ''}
                            color="primary"
                        >
                            Submit
                        </Button>
                    </div>
                </div>
            </form>
        </div>
    );
}

export default SearchBar;