import React, {useEffect, useState} from 'react';
import MediaCard from "../components/MediaCard";
import {useLocation} from "react-router-dom";
import SearchBar from "../components/SearchBar";
import {searchMedia} from "../api/apiCalls";

const Media = ({bookmarks, setBookmarks}) => {

    const [media, setMedia] = useState([]);
    const [searchTerm, setSearchTerm] = useState("");
    const [type, setType] = useState("");
    const [doesResultExist, setDoesResultExist] = useState(true);
    const location = useLocation();

    useEffect(() => {
        setMedia([]);
        setSearchTerm("");
        setDoesResultExist(true);
        location.pathname === '/' ? setType("Movies") : location.pathname === '/series' && setType("Series")
    }, [location])

    const search = async (term) => {
        const resultList = await searchMedia(type, term);
        resultList.length === 0 ? setDoesResultExist(false) : setDoesResultExist(true);
        setMedia(resultList);
    }

    return (
        <div className="ui container">
            <div className="results-container">
                <SearchBar
                    onFormSubmit={search}
                    type={type}
                    searchTerm={searchTerm}
                    setSearchTerm={setSearchTerm}
                />
                {doesResultExist ?
                    media.map(item => {
                        return (
                            <MediaCard
                                key={item.id}
                                media={item}
                                bookmarks={bookmarks}
                                setBookmarks={setBookmarks}
                            />
                        );
                    })
                    :
                    <h2 className="media__no-result">
                        No {type} found
                    </h2>
                }
            </div>
        </div>
    );
}

export default Media;