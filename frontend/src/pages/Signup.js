import React, {useState} from "react";
import formIsValid from "../validation/validate";
import {Link} from "react-router-dom";
import {signupUser} from "../api/apiCalls";

const SignUp = ({handleUserAuthentication}) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState(password);
    const [doesEmailAlreadyExist, setDoesEmailAlreadyExist] = useState(false);
    const [arePasswordsIdentical, setArePasswordsIdentical] = useState(true);

    const signup = async (e) => {
        e.preventDefault();
        if (formIsValid(username, password, confirmPassword)) {
            let userForSignup = {
                username,
                password
            };
            setArePasswordsIdentical(true)
            const response = await signupUser(userForSignup);
            if (response === 208) {
                setDoesEmailAlreadyExist(true)
            } else {
                setDoesEmailAlreadyExist(false);
                handleUserAuthentication(response);
            }
        } else {
            setArePasswordsIdentical(false)
        }
    }

    return (
        <div className="auth-wrapper">
            <div className="auth-inner">
                <form onSubmit={signup}>
                    <h3>
                        Sign Up
                    </h3>
                    <div className="form-group">
                        <label>
                            Email address
                        </label>
                        <input
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            type="email"
                            className="form-control"
                            placeholder="Enter email"
                        />
                    </div>
                    <div className="form-group">
                        <label>
                            Password
                        </label>
                        <input
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            type="password"
                            className="form-control"
                            placeholder="Enter password"
                        />
                    </div>
                    <div className="form-group">
                        <label>
                            Confirm Password
                        </label>
                        <input
                            onChange={(e) => setConfirmPassword(e.target.value)}
                            value={confirmPassword}
                            type="password"
                            id="confirmPw"
                            className="form-control"
                            placeholder="Confirm password"
                        />
                    </div>
                    <button
                        type="submit"
                        className="btn btn-primary btn-block"
                    >
                        Sign Up
                    </button>
                    {doesEmailAlreadyExist ? <div className="signup__error"> Email already in use </div> : null}
                    {!arePasswordsIdentical ? <div className="signup__error"> Passwords are not identical </div> : null}
                    <p className="signup__link">
                        Already registered? <Link to="/login"> Log in! </Link>
                    </p>
                </form>
            </div>
        </div>
    );
}

export default SignUp;
