import React from 'react';
import MediaCard from "../components/MediaCard";

const Bookmarks = ({bookmarks, setBookmarks}) => {

    return (
        <div className="ui container">
            {bookmarks.length !== 0 ?
                bookmarks.map(bookmark => {
                    return (
                        <MediaCard
                            key={bookmark.id}
                            bookmarks={bookmarks}
                            setBookmarks={setBookmarks}
                            media={bookmark}
                        />
                    )
                })
                :
                <h2 className="media__no-result">
                    You haven't added any bookmarks yet!
                </h2>
            }
        </div>
    );
}

export default Bookmarks;