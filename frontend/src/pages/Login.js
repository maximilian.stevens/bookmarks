import React, {useState} from "react";
import formIsValid from "../validation/validate";
import {Link} from "react-router-dom";
import {loginUser} from "../api/apiCalls";

const Login = ({handleUserAuthentication}) => {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [isUserFound, setIsUserFound] = useState(true);

    const login = async (e) => {
        e.preventDefault();
        if (formIsValid(username, password, password)) {
            let userForLogin = {
                username,
                password,
            };
            const user = await loginUser(userForLogin);
            if (user !== null) {
                setIsUserFound(true);
                handleUserAuthentication(user);
            } else {
                setIsUserFound(false)
            }
        }
    }

    return (
        <div className="auth-wrapper">
            <div className="auth-inner">
                <form onSubmit={login}>
                    <h3>
                        Login
                    </h3>
                    <div className="form-group">
                        <label>
                            Email address
                        </label>
                        <input
                            onChange={(e) => setUsername(e.target.value)}
                            value={username}
                            type="email"
                            className="form-control"
                            placeholder="Enter email"
                        />
                    </div>
                    <div className="form-group">
                        <label>
                            Password
                        </label>
                        <input
                            onChange={(e) => setPassword(e.target.value)}
                            value={password}
                            type="password"
                            className="form-control"
                            placeholder="Enter password"
                        />
                    </div>
                    <button
                        type="submit"
                        className="btn btn-primary btn-block"
                    >
                        Submit
                    </button>
                    {!isUserFound ? <div className="login__error"> Wrong Email or Password</div> : null}
                    <p className="login__link">
                        New to us? <Link to="/sign-up"> Sign up!</Link>
                    </p>
                </form>
            </div>
        </div>
    );
}

export default Login;
