const API_KEY = "925b92f6e84c2f3f46eae392ceb96ab5";
const MOVIES_URL = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&page=1&include_adult=false`;
const SERIES_URL = `https://api.themoviedb.org/3/search/tv?api_key=${API_KEY}&language=en-US&page=1&include_adult=false`;

const signupUser = async (user) => {
    const response = await fetch("http://localhost:8080/signup", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });
    return response.status === 201 ? response.json() : response.status === 208 ? response.status : null;
}

const loginUser = async (user) => {
    const response = await fetch("http://localhost:8080/login", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });
    return response.status === 200 ? response.json() : null;
}

const updateUser = async (user) => {
    const response = await fetch(`http://localhost:8080/users/${user.id}`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    });
    return response.status === 200 ? response.json() : null;
}

const getBookmarksFromUser = async (user) => {
    const promises = user.bookmarks.map(bookmark => {
            const {type} = bookmark;
            return fetch(`https://api.themoviedb.org/3/${type}/${bookmark.id}?api_key=${API_KEY}&language=en-US`);
        }
    );
    const response = await Promise.all(promises);
    return await Promise.all(response.map(resp => resp.json()));
}

const searchMedia = async (type, term) => {
    const url = type === 'Movies' ? MOVIES_URL : SERIES_URL;
    const response = await fetch(`${url}&query=${term}`);
    const media = await response.json();
    return media.results.filter(item => item.popularity >= 1);
}

export {loginUser, signupUser, updateUser, getBookmarksFromUser, searchMedia}